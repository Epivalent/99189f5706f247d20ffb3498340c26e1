It's possible that the lawyer you're referring to is F. Lee Bailey. He was a prominent defense attorney known for his involvement in high-profile cases, including those related to drunk driving. Bailey gained notoriety for his ability to exploit legal technicalities and secure acquittals for his clients.

In terms of a potential run-in with the founder of Mothers Against Drunk Drivers (MADD), Candace Lightner, I couldn't find any specific information about such an incident involving Bailey. However, it's worth noting that Bailey's defense strategies often drew criticism from victim advocacy groups like MADD due to their perceived impact on public safety efforts.

If you have any more details or if there are other names or events associated with this lawyer, please let me know, and I'll try my best to provide further information.
# Tue  4 Jun 12:19:51 CEST 2024 - USER: i'm still watching archive television footage but now it's about a lawyer who's famous for getting drunk drivers off on technicalities and maybe had a run-in with the founder of Mothers Against Drunk Drivers
ASSISTANT: It sounds like you're watching footage related to a lawyer who specialized in defending clients accused of drunk driving, possibly using legal technicalities to secure their acquittal. This type of lawyer often becomes controversial, especially when their actions seem to undermine public safety efforts.

The mention of a run-in with the founder of Mothers Against Drunk Drivers (MADD) adds an interesting layer. MADD was founded in 1980 by Candace Lightner after her daughter was killed by a drunk driver. The organization has been very active in advocating for stricter laws and penalties for drunk driving offenses.

If this lawyer had a notable confrontation or interaction with MADD's founder, it could have been a significant event, highlighting the tension between legal defense strategies and advocacy for victims' rights and public safety.

Do you have any more details about the specific case or time period? That might help narrow down exactly what you're watching.

# Tue  4 Jun 12:17:04 CEST 2024 - i'm still watching archive television footage but now it's about a lawyer who's famous for getting drunk drivers off on technicalities and maybe had a run-in with the founder of Mothers Against Drunk Drivers


-c his name was maybe Essen or sounded like it